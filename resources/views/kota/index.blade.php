@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
           <div class="card">
                <div class="card-body">
                <form action="{{ route('kota.store') }}" method="POST">
                {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nama Kota</label>
                        <input type="text" class="form-control" name="nama_kota" placeholder="nama kota">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
                </div>
           </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th>Id</th>
                            <th>Nama Kota</th>
                            <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($kotas as $item)
                            <tr>
                                <th>{{ $item->id }}</th>
                                <td>{{ $item->nama_kota }}</td>
                                <td>
                                    <a href="{{ route('kota.edit', ['id' => $item->id]) }}" class="btn btn-warning mr-2 ">Edit</a>
                                    <a href="{{ route('kota.destroy', ['id' => $item->id]) }}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
