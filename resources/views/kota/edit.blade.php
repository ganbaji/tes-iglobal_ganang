@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
           <div class="card">
                <div class="card-body">
                <form action="{{ route('kota.update' , ['id' => $kota->id]) }}" method="POST">
                {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nama Kota</label>
                        <input type="text" class="form-control" name="nama_kota" placeholder="nama kota" value="{{ $kota->nama_kota }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success mr-2" type="submit">Update</button>
                        <a href="{{ route('kota.index') }}" class="btn btn-secondary">Kembali</a>
                    </div>
                </form>
                </div>
           </div>
        </div>
    </div>
</div>
@endsection