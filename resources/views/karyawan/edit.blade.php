@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
           <div class="card">
                <div class="card-body">
                <form action="{{ route('karyawan.update' , ['id' => $karyawan->id]) }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleFormControlInput1">Nama Karyawan</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="nama_karyawan" value="{{ $karyawan->nama_karyawan}}">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">No KTP</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="no_ktp" value="{{ $karyawan->no_ktp}}">
                </div><div class="form-group">
                    <label for="exampleFormControlInput1">Jabatan</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="jabatan" value="{{ $karyawan->jabatan}}">
                </div><div class="form-group">
                    <label for="exampleFormControlInput1">Bidang</label>
                    <select name="bidang" class="form-control" >
                        @foreach ($bidangs as $item)
                        <option value="{{ $item->id }}" {{ $item->id == $karyawan->bidang ? 'selected' : '' }}>{{ $item->nama_bidang }}</option>
                        @endforeach
                    </select>
                </div><div class="form-group">
                    <label for="exampleFormControlInput1">Sub Bidang</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="sub_bidang" value="{{ $karyawan->sub_bidang}}">
                </div><div class="form-group">
                    <label for="exampleFormControlInput1">Kota</label>
                    <select name="kota" class="form-control" >
                        @foreach ($kotas as $item)
                        <option value="{{ $item->id }}" {{ $item->id == $karyawan->kota ? 'selected' : '' }}>{{ $item->nama_kota }}</option>
                        @endforeach
                    </select>
                </div><div class="form-group">
                    <label for="exampleFormControlInput1">Alamat</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="alamat" value="{{ $karyawan->alamat}}">
                </div>
                    <div class="form-group">
                        <button class="btn btn-success mr-2" type="submit">Update</button>
                        <a href="{{ route('karyawan.index') }}" class="btn btn-secondary">Kembali</a>
                    </div>
                </form>
                </div>
           </div>
        </div>
    </div>
</div>
@endsection