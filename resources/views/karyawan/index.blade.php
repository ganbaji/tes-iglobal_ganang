@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
           <div class="card">
                <div class="card-body">
                <form action="{{ route('karyawan.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nama Karyawan</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="nama_karyawan" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">No KTP</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="no_ktp" placeholder="">
                    </div><div class="form-group">
                        <label for="exampleFormControlInput1">Jabatan</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="jabatan" placeholder="">
                    </div><div class="form-group">
                        <label for="exampleFormControlInput1">Bidang</label>
                        <select name="bidang" class="form-control" >
                            @foreach ($bidangs as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_bidang }}</option>
                            @endforeach
                        </select>
                    </div><div class="form-group">
                        <label for="exampleFormControlInput1">Sub Bidang</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="sub_bidang" placeholder="">
                    </div><div class="form-group">
                        <label for="exampleFormControlInput1">Kota</label>
                        <select name="kota" class="form-control" >
                            @foreach ($kotas as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_kota }}</option>
                            @endforeach
                        </select>
                    </div><div class="form-group">
                        <label for="exampleFormControlInput1">Alamat</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="alamat" placeholder="">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
                </div>
           </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="" method="get" class="mb-4">
                       <div class="row">
                           <div class="col-6">
                            <input type="text" class="form-control" name="cari" placeholder="cari data">
                        </div>
                        <div class="col-6">
                               <button type="submit" class="btn btn-info">Cari</button>
                           </div>
                       </div>
                    </form>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th>Id</th>
                            <th>Nama</th>
                            <th>No KTP</th>
                            <th>Jabatan</th>
                            <th>Bidang</th>
                            <th>Sub bidang</th>
                            <th>Kota</th>
                            <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($karyawans as $item)
                        <tr>
                            <th>{{$item->id}}</th>
                            <th>{{$item->nama_karyawan}}</th>
                            <th>{{$item->no_ktp}}</th>
                            <th>{{$item->jabatan}}</th>
                            <th>{{$item->nama_bidang}}</th>
                            <th>{{$item->sub_bidang}}</th>
                            <th>{{$item->nama_kota}}</th>
                            <td>
                                    <a href="{{ route('karyawan.edit', ['id' =>$item->id]) }}" class="btn btn-warning mr-2 ">Edit</a>
                                    <a href="{{ route('karyawan.destroy', ['id' => $item->id]) }}" class="btn btn-danger">Delete</a>
                                </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $karyawans->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
