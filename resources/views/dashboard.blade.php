@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <p>Total Karyawan</p>
                    <p>{{ $total_karyawan }}</p>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <p>Total Bidang</p>
                    <p>{{ $total_bidang }}</p>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <p>Total Kota</p>
                    <p>{{ $total_kota }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
