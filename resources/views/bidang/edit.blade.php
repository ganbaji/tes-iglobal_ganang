@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
           <div class="card">
                <div class="card-body">
                <form action="{{ route('bidang.update' , ['id' => $bidang->id]) }}" method="POST">
                {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nama Bidang</label>
                        <input type="text" class="form-control" name="nama_bidang" placeholder="nama bidang" value="{{ $bidang->nama_bidang }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success mr-2" type="submit">Update</button>
                        <a href="{{ route('bidang.index') }}" class="btn btn-secondary">Kembali</a>
                    </div>
                </form>
                </div>
           </div>
        </div>
    </div>
</div>
@endsection