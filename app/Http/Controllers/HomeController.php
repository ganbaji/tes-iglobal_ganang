<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kota;
use App\Models\Karyawan;
use App\Models\Bidang;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['total_karyawan'] = Karyawan::count();
        $data['total_bidang'] = Bidang::count();
        $data['total_kota'] = Kota::count();
        return view('dashboard', $data);
    }
}
