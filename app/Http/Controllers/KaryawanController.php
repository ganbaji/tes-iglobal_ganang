<?php

namespace App\Http\Controllers;
use App\Models\Karyawan;
use App\Models\Bidang;
use App\Models\Kota;

use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari_data = $request->get('cari');

        if ($cari_data) {
            $data['karyawans'] = Karyawan::join('bidang','karyawan.bidang','bidang.id')
                                        ->join('kota','karyawan.kota','kota.id')
                                        ->select('bidang.nama_bidang','kota.nama_kota','karyawan.*')
                                        ->where('karyawan.nama_karyawan','LIKE','%'.$cari_data .'%')
                                        ->orWhere('karyawan.jabatan','LIKE','%'.$cari_data .'%')
                                        ->orWhere('bidang.nama_bidang','LIKE','%'.$cari_data .'%')
                                        ->paginate(1);
        } else {
            $data['karyawans'] = Karyawan::join('bidang','karyawan.bidang','bidang.id')
                                         ->join('kota','karyawan.kota','kota.id')
                                         ->select('bidang.nama_bidang','kota.nama_kota','karyawan.*')
                                         ->paginate(1);
        }
        $data['bidangs'] = Bidang::get();
        $data['kotas'] = Kota::get();
        return view('karyawan.index', $data);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_karyawan = new Karyawan;
        $new_karyawan->nama_karyawan = $request->get('nama_karyawan');
        $new_karyawan->no_ktp = $request->get('no_ktp');
        $new_karyawan->jabatan = $request->get('jabatan');
        $new_karyawan->bidang = $request->get('bidang');
        $new_karyawan->sub_bidang = $request->get('sub_bidang');
        $new_karyawan->kota = $request->get('kota');
        $new_karyawan->alamat = $request->get('alamat');
        $new_karyawan->save();
        return redirect()->back();
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['karyawan'] = Karyawan::find($id);
        $data['bidangs'] = Bidang::get();
        $data['kotas'] = Kota::get();

        return view('karyawan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_data = Karyawan::find($id);
        $update_data->nama_karyawan = $request->get('nama_karyawan');
        $update_data->no_ktp = $request->get('no_ktp');
        $update_data->jabatan = $request->get('jabatan');
        $update_data->bidang = $request->get('bidang');
        $update_data->sub_bidang = $request->get('sub_bidang');
        $update_data->bidang = $request->get('kota');
        $update_data->alamat = $request->get('alamat');
        $update_data->save();

        return redirect()->route('karyawan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_data = Karyawan::find($id);
        $delete_data->delete();

        return redirect()->back();
    }
}
