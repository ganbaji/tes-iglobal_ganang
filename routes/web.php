<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');

Route::get('/karyawan', 'KaryawanController@index')->name('karyawan.index');
Route::post('/karyawan/store', 'KaryawanController@store')->name('karyawan.store');
Route::get('/karyawan/edit/{id}', 'KaryawanController@edit')->name('karyawan.edit');
Route::post('/karyawan/update/{id}', 'KaryawanController@update')->name('karyawan.update');
Route::get('/karyawan/delete/{id}', 'KaryawanController@destroy')->name('karyawan.destroy');

Route::get('/bidang', 'BidangController@index')->name('bidang.index');
Route::post('/bidang/store', 'BidangController@store')->name('bidang.store');
Route::get('/bidang/edit/{id}', 'BidangController@edit')->name('bidang.edit');
Route::post('/bidang/update/{id}', 'BidangController@update')->name('bidang.update');
Route::get('/bidang/delete/{id}', 'BidangController@destroy')->name('bidang.destroy');

Route::get('/kota', 'KotaController@index')->name('kota.index');
Route::post('/kota/store', 'KotaController@store')->name('kota.store');
Route::get('/kota/edit/{id}', 'KotaController@edit')->name('kota.edit');
Route::post('/kota/update/{id}', 'KotaController@update')->name('kota.update');
Route::get('/kota/delete/{id}', 'KotaController@destroy')->name('kota.destroy');
